Photo App
===========
Min API level is set to [`21`](https://android-arsenal.com/api?level=21)

Libraries Used
-------------

* [Animations & Transitions](https://developer.android.com/training/animation/) - Move widgets and transition between screens.
* [Navigation](https://developer.android.com/guide/navigation) - Handle everything needed for in-app navigation.

Screenshots
-----------

![image](screen.png)![image](screen2.png)![image](screen3.png)

![navigation](screen4.png)