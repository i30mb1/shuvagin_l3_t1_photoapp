package com.example.shuvagin_l3_t1


import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import android.transition.Slide
import android.transition.TransitionInflater
import com.example.shuvagin_l3_t1.databinding.FragmentItemHistoryFullBinding


/**
 * A simple [Fragment] subclass.
 */
class ItemHistoryFullFragment : Fragment() {

    val args: ItemHistoryFullFragmentArgs by navArgs()
    lateinit var binding: FragmentItemHistoryFullBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_history_full, container, false)
       Slide().apply {
            slideEdge = Gravity.BOTTOM
            duration = 300
            addTarget(binding.bFragmentItemHistoryFullAdd)
            enterTransition = this
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.item = args.item
        binding.bFragmentItemHistoryFullAdd.setOnClickListener {
            Toast.makeText(context, getString(R.string.history_was_added), Toast.LENGTH_SHORT).show()
        }

        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        returnTransition =TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        exitTransition =TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }
}
