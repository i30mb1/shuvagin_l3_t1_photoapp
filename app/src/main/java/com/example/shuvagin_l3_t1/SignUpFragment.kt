package com.example.shuvagin_l3_t1


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.example.shuvagin_l3_t1.SignInFragment.Companion.animationFun
import com.example.shuvagin_l3_t1.databinding.FragmentSignUpBinding

/**
 * A simple [Fragment] subclass.
 */
class SignUpFragment : Fragment() {

    lateinit var binding:FragmentSignUpBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_sign_up,container,false)
        sharedElementEnterTransition = animationFun
        sharedElementReturnTransition = animationFun
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bActivityWelcomeSignUp.setOnClickListener{findNavController().navigate(R.id.action_signUpFragment_to_historyFragment)}
    }
}
