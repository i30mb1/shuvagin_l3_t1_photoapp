package com.example.shuvagin_l3_t1


import android.content.SharedPreferences
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import com.example.shuvagin_l3_t1.SignInFragment.Companion.EMAIL
import com.example.shuvagin_l3_t1.SignInFragment.Companion.PASSWORD
import com.example.shuvagin_l3_t1.databinding.FragmentWelcomeBinding

/**
 * A simple [Fragment] subclass.
 */
class WelcomeFragment : Fragment() {

    lateinit var binding: FragmentWelcomeBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome, container, false)
        binding.fragment = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    fun navigateToSignIn(v: View) {
        val email = PreferenceManager.getDefaultSharedPreferences(context).getString(EMAIL, "")
        val password = PreferenceManager.getDefaultSharedPreferences(context).getString(PASSWORD, "")

        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            val extras = FragmentNavigatorExtras(binding.bActivityWelcomeSignIn to getString(R.string.transition_sign_in))
            findNavController().navigate(R.id.action_welcomeFragment_to_signIpFragment, null, null, extras)
        } else {
            findNavController().navigate(R.id.action_welcomeFragment_to_historyFragment)
        }
    }

    fun navigateToSignUp(v: View) {
        val extras = FragmentNavigatorExtras(binding.bActivityWelcomeSignUp to getString(R.string.transition_sign_up))
        findNavController().navigate(R.id.action_welcomeFragment_to_signUpFragment, null, null, extras)
    }

}
