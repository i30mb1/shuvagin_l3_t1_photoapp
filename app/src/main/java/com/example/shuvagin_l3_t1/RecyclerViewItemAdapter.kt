package com.example.shuvagin_l3_t1

import android.os.Parcelable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Keep
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.RecyclerView
import com.example.shuvagin_l3_t1.databinding.ItemHistoryBinding
import kotlinx.android.parcel.Parcelize

@Keep
@Parcelize
data class Item(val headline: String = "", val body: String = "") : Parcelable

class RecyclerViewItemAdapter : RecyclerView.Adapter<RecyclerViewItemAdapter.ViewHolder>() {

    private var list: List<Item> = List(1) { Item() }

    fun setList(list: List<Item>) {
      this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: ItemHistoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_history,
            parent,
            false
        )
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.set(list[position])
    }

    inner class ViewHolder(val itemHistoryBinding: ItemHistoryBinding) : RecyclerView.ViewHolder(itemHistoryBinding.root) {

        fun set(item: Item) {
            itemHistoryBinding.tvItemHistoryH6.transitionName = ""
            itemHistoryBinding.tvItemHistoryBody.transitionName = ""
            itemHistoryBinding.cvItemHistory.transitionName = ""
            itemHistoryBinding.item = item
            itemHistoryBinding.root.setOnClickListener { launchWithAnimation(item) }
            itemHistoryBinding.executePendingBindings()
        }

        /**
         *
         */
        private fun launchWithAnimation(item: Item) {
            val transitionNameHeader = itemHistoryBinding.root.context.resources.getString(R.string.tn_item_history_h6)
            itemHistoryBinding.tvItemHistoryH6.transitionName = transitionNameHeader
            val transitionNameBody = itemHistoryBinding.root.context.resources.getString(R.string.tn_item_history_body)
            itemHistoryBinding.tvItemHistoryBody.transitionName = transitionNameBody
            val transitionNameCard = itemHistoryBinding.root.context.resources.getString(R.string.tn_item_history_card)
            itemHistoryBinding.cvItemHistory.transitionName = transitionNameCard
            val extras = FragmentNavigatorExtras(
                itemHistoryBinding.cvItemHistory to transitionNameCard,
                itemHistoryBinding.tvItemHistoryH6 to transitionNameHeader,
                itemHistoryBinding.tvItemHistoryBody to transitionNameBody
            )
            val action = HistoryFragmentDirections.actionHistoryFragmentToItemHistoryFullFragment(item)
            Navigation.findNavController(itemHistoryBinding.root)
                .navigate(action, extras)
        }
    }
}