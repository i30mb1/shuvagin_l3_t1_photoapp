package com.example.shuvagin_l3_t1


import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.transition.ChangeBounds
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.BounceInterpolator
import androidx.core.content.edit
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.preference.Preference
import androidx.preference.PreferenceDataStore
import androidx.preference.PreferenceManager
import com.example.shuvagin_l3_t1.databinding.FragmentSignInBinding

/**
 * A simple [Fragment] subclass.
 */
class SignInFragment : Fragment() {

    lateinit var binding: FragmentSignInBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        sharedElementEnterTransition = animationFun
        sharedElementReturnTransition = animationFun
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.bFragmentSignIn.setOnClickListener {
            saveUserFieldsIfNeed()
            findNavController().navigate(R.id.action_signIpFragment_to_historyFragment)
        }
    }

    private fun saveUserFieldsIfNeed() {
        PreferenceManager.getDefaultSharedPreferences(context).edit {
            putString(EMAIL,binding.etFragmentSignInEmail.text.toString())
            putString(PASSWORD,binding.etFragmentSignInPassword.text.toString())
        }
//        context?.getSharedPreferences(PREFERENCE_USER,MODE_PRIVATE)?.edit {
//
//        }
    }

    companion object {
        const val ANIMATION_DURATION = 800L
        const val PREFERENCE_USER = "com.example.shuvagin_l3_t1.PREFERENCE_USER"
        const val EMAIL = "EMAIL"
        const val PASSWORD = "PASSWORD"
        internal val animationFun = ChangeBounds().apply {
            duration = ANIMATION_DURATION
            interpolator = BounceInterpolator()
        }
    }

    class DataStore : PreferenceDataStore() {
        override fun putString(key: String?, value: String?) {
            super.putString(key, value)
        }

        override fun getString(key: String?, defValue: String?): String? {
            return super.getString(key, defValue)
        }
    }

}
